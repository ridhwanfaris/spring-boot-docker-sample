package com.faris.springbootdocker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/message")
	public String showMessage() {
		return "Welcome Docker..!!";
	}
}
